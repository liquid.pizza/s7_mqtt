import ledHandler

def showValue(v):
    v_str = str(v)
    l = len(v_str)

    if l > ledHandler.NUM_OF_SEGMENTS:
        return

    digits = [(int(d), l-i-1) for i, d in enumerate(v_str)]
    print(f"{v}: {digits}")

    for d, i in digits:
        reset_leds = i==len(digits)-1
        print(f"Set {d} in position {i} - {reset_leds}")
        ledHandler.show(d, i, reset_leds)

    ledHandler.write_leds()