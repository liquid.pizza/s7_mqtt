from apa106 import APA106
from machine import Pin
from time import sleep

PIN_NUM = 4
LED_PER_SEGMENT = 3
BRIGHTNESS = 255

ap = APA106(Pin(PIN_NUM), ARRAY_LEN)

num_to_seg_map ={
    0: [0, 1, 2, 3, 4, 5],
    1: [4, 5],
    2: [0, 1, 3, 4, 6],
    3: [0, 3, 4, 5, 6],
    4: [2, 4, 5, 6],
    5: [0, 2, 3, 5, 6],
    6: [0, 1, 2, 3, 5, 6],
    7: [3, 4, 5],
    8: [0, 1, 2, 3, 4, 5, 6],
    9: [0, 2, 3, 4, 5, 6,],
}

def map_segment_to_led(n):
    return (n//LED_PER_SEGMENT, n%LED_PER_SEGMENT)

def show(n, offset=0, reset_leds=True):
    segments = num_to_seg_map[n]

    led_offset = offset*LED_PER_SEGMENT

    # print(f"segments:{segments}")

    if reset_leds:
        ap.fill((0,0,0))

    for s in segments:
        led, channel = map_segment_to_led(s)

        led_index = led + led_offset

        print(f"segment: {s} -> {led_index}, {channel}")
        col = list(ap[led_index])
        print(f"col before: {col}")
        col[channel] = BRIGHTNESS
        print(f"col after: {col}")
        ap[led_index] = col
        print(f"ap[led_index] after: {ap[led_index]}")
        print(f"segment: {s} -> {led}, {channel}")
        col = list(ap[led])
        col[channel] = 255
        ap[led + offset*LED_PER_SEGMENT] = col
    
    ap.write()

def count(n=10):
    for i in range(n):
        show(i)
        sleep(1)